const test = require('tape')
const Server = require('../test-bot')
const { isMsgId } = require('ssb-ref')

test('settings.tombstone', { objectPrintDepth: 6 }, async t => {
  const server = await Server()

  const settings = {
    authors: { add: [server.id] },
    keyBackedUp: false
  }

  const settingsId = await server.settings.create(settings)
    .catch(t.fail)

  t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')

  let data = await server.settings.get(settingsId)
    .catch(t.fail)

  const expectedState = {
    keyBackedUp: false,
    tombstone: null,
    authors: { [server.id]: [{ start: 0, end: null }] }
  }
  const expectedSettings = {
    key: settingsId,
    type: 'settings',
    originalAuthor: server.id,
    recps: null,
    states: [{
      key: settingsId,
      ...expectedState
    }],
    ...expectedState,
    conflictFields: []
  }
  t.deepEqual(data, expectedSettings, 'Data was correct')

  const opts = {
    reason: 'No longer wanted'
  }

  const tombstoneId = await server.settings.tombstone(settingsId, opts)
    .catch(t.fail)

  t.true(isMsgId(tombstoneId), 'Tombsone return valid message Id')

  data = await server.settings.get(settingsId)
    .catch(t.fail)

  const expectedTombstoneSettings = expectedSettings
  const tombstone = {
    date: data.states[0].tombstone.date, // Hack, can't test dates
    reason: 'No longer wanted'
  }
  expectedTombstoneSettings.states[0] = {
    ...expectedTombstoneSettings.states[0],
    key: tombstoneId,
    tombstone
  }
  expectedTombstoneSettings.tombstone = tombstone

  t.deepEqual(data, expectedTombstoneSettings, 'Settings were tombstoned')

  server.close()
  t.end()
})
