const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

test('settings.link.create and findByFeedId', t => {
  const server = Server({
    recpsGuard: true
  })

  const settings = {
    authors: { add: [server.id] },
    keyBackedUp: false,
    recps: [server.id]
  }

  // Create settings
  server.settings.create(settings, (err, settingsId) => {
    t.error(err, 'Create settings without error')
    t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')

    server.settings.link.create({ settings: settingsId }, (err, linkId) => {
      t.error(err, 'Create link without error')
      t.true(isMsgId(linkId), 'Is valid scuttlebutt id')

      server.settings.findByFeedId(server.id, (err, allSettings) => {
        t.error(err, 'Find settings by feed id without error')

        const expectedSettings = [{
          key: settingsId,
          type: 'settings',
          originalAuthor: server.id,
          recps: [server.id],
          states: [{
            key: settingsId,
            tombstone: null,
            keyBackedUp: false,
            authors: {
              [server.id]: [{
                start: 0,
                end: null
              }]
            }
          }]
        }]

        t.deepEqual(expectedSettings, allSettings, 'Returned settings were correct')

        server.close()
        t.end()
      })
    })
  })
})
