const test = require('tape')
const Server = require('../test-bot')
const { isMsgId } = require('ssb-ref')

test('settings.get', { objectPrintDepth: 6 }, async t => {
  const server = await Server()

  const settings = {
    authors: { add: [server.id] },
    keyBackedUp: false
  }

  const settingsId = await server.settings.create(settings)
    .catch(t.fail)

  t.true(isMsgId(settingsId), 'Is valid scuttlebutt id')

  let data = await server.settings.get(settingsId)
    .catch(t.fail)

  const state = {
    keyBackedUp: false,
    tombstone: null,
    authors: { [server.id]: [{ start: 0, end: null }] }
  }
  const expectedSettings = {
    key: settingsId,
    type: 'settings',
    originalAuthor: server.id,
    recps: null,
    states: [{
      key: settingsId,
      ...state
    }],
    ...state,
    conflictFields: []
  }
  t.deepEqual(data, expectedSettings, 'Data was correct')

  const update = {
    keyBackedUp: true
  }

  const updateId = await server.settings.update(settingsId, update)
    .catch(t.fail)

  t.true(isMsgId(updateId), 'Update return valid message Id')

  data = await server.settings.get(settingsId)
    .catch(t.fail)

  const expectedUpdatedSettings = expectedSettings
  expectedUpdatedSettings.states[0].key = updateId
  expectedUpdatedSettings.states[0].keyBackedUp = true
  expectedUpdatedSettings.keyBackedUp = true

  t.deepEqual(data, expectedUpdatedSettings, 'Updated data was correct')

  server.close()
  t.end()
})
