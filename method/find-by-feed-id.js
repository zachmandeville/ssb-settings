const { isFeed } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')

module.exports = function FindByFeedId (server, getSettings) {
  const getActiveSettings = (settingsId, cb) => {
    getSettings(settingsId, (err, settings) => {
      if (err) return cb(null, null)
      if (settings.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" settings means no settings / tombstoned

      cb(null, settings)
    })
  }

  return function findByFeedId (feedId, opts = {}, cb) {
    if (typeof opts === 'function') return findByFeedId(feedId, {}, opts)
    if (!isFeed(feedId)) return cb(new Error('requires a valid feedId'))

    const query = [{
      $filter: {
        dest: feedId,
        value: {
          author: feedId, // message published about feedId and BY same person
          content: {
            type: 'link/feed-settings',
            parent: feedId,
            tangles: {
              link: { root: null, previous: null }
            }
          }
        }
      }
    }]

    pull(
      server.backlinks.read({ query }),
      pull.map(link => link.value.content.child),
      paraMap(opts.getSettings || getActiveSettings, 4),
      pull.filter(Boolean),
      pull.collect((err, results) => {
        if (err) return cb(err)

        cb(null, results)
      })
    )
  }
}
