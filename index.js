const API = require('./method')

module.exports = {
  name: 'settings',
  version: require('./package.json').version,
  manifest: {
    create: 'async',
    get: 'async',
    update: 'async',
    tombstone: 'async'
  },
  init: (server) => {
    if (!server.backlinks) throw new Error('ssb-settings requires ssb-backlinks')
    return API(server)
  }
}
